<?php

namespace App\Lib;
/**
 * @author ewanjau
 */
class Config
{
    /** Database host name or IP address.
     * @var string
     */
    const HOST = "localhost";

    /** Database user name.
     * @var string
     */
    const USER = "ets_user";
    
    const PASSWORD = "123qweASD";
    /** Database name.
     * @var string
     */
    const DATABASE = "esd_signatures";   
    /** Database port in use.
     * @var string*/
    const PORT = "3306";
    const INFO_LOG_PATH = "/var/log/applications/esdhandler/info.log";
    const TAT_LOG_PATH = "/var/log/applications/esdhandler/tat_info.log";
    const ERROR_LOG_PATH = "/var/log/applications/esdhandler/error.log";
    const FATAL_LOG_PATH = "/var/log/applications/esdhandler/fatal.log";
    const DEBUG_LOG_PATH = "/var/log/applications/esdhandler/debug.log";
    
    ######## Please do not change #############
    const STATUS_SUCCESS=200;
    const STATUS_GENERIC_FAILURE=500;
    const STATUS_MYSQL_DUPLICATE_RECORD=23000;
    static $VALIDATION_PARAMS=array(
            'logESDRequest'=>array(
               'orderNo', 'amount','MSISDN','beepTransactionID',));
    const DEFAULT_COMPANY_ID=1;
    const STATUS_UNPROCESSED=0;
}