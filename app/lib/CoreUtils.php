<?php

//declare the namespace
namespace App\Lib;

//autoload the classes
require_once __DIR__ . '/../../vendor/autoload.php';

//reference classes from the global namespace
use \Exception;

//reference classes from our namespace
use \App\Lib\Db\MySQL;
use \App\Lib\Db\DatabaseUtilities;
use \App\Lib\Db\SQLException;


class CoreUtils
{
    /**
     * Receives the post and decodes the JSON string
     *
     * @return array the decoded JSON string aor an appropriate error
     * status if an error occurred
     */
    public static function receivePost()
    {
        $jsonPost = file_get_contents('php://input');
        static::flog(
            'Received Payload: ' .
            static::printArray($jsonPost),'',__METHOD__,__LINE__,
            Config::DEBUG_LOG_PATH);

        if (empty($jsonPost)) {
            static::flog(
                'Payload is null,Cannot continue processing','',
            __METHOD__,__LINE__,   Config::ERROR_LOG_PATH);

            //No post data
            $status['statusCode'] = Config::STATUS_GENERIC_FAILURE;
            $status['statusDescription'] = 'Payload not received successfully
            on API';
            $response['status'] = $status;
            $response['results'] = array();

            return $response;
        } else {
            $response = json_decode($jsonPost, true);

            if ($response != null || $response != false) {
                return $response;
            } else {
                //cannot decode the payload
                $status['statusCode'] = Config::STATUS_GENERIC_FAILURE;
                $status['statusDescription'] = 'Internal API server error
                .Payload cannot be decoded.Please contact Cellulant support';

                $response['status'] = $status;
                $response['results'] = array();
                return $response;
            }
        }
    }

    /**
     * Replaces the ? with the right parameter for logging purposes.
     *
     * @param string $query the parametrized SQL query
     * @param array $params the SQL query parameters
     *
     * @return string the complete query for logging purposes
     */
    public static function formatQueryForLogging($query, $params)
    {
        try {
            /*
             * Divide the string using the ? delimeter so we can replace
             * correctly.
             */
            $buffer = explode('?', $query);

            $c = count($params);
            for ($index = 1; $index < $c; $index++) {
                /*
                 * Starts from index 1 to ignore the first param. Append back
                 * the ? after it was removed by the explode method.
                 */
                $sub = $buffer[$index - 1] . '?';

                // In each sub string replace the ?
                $buffer[$index - 1] = str_replace('?', $params[$index], $sub);
            }

            // ReConstruct the string
            return implode("", $buffer);
        } catch (Exception $ex) {
            // If any thing goes wrong return the original string
            return $query;
        }
    }


    /**
     * Utility function for printing out a preparedStatement during logging
     * @param $preparedStatement
     * @return string
     */
    public static function formatPrepStmtForLogging($preparedStatement)
    {
        ob_start();
        $preparedStatement->debugDumpParams();
        $queryDump = ob_get_clean();
        return $queryDump;
    }

    
    
    /**
     * 
     * @param string $string
     * @param string $msisdn
     * @param string $method
     * @param int $line
     * @param int $logPath
     */
    public static function flog($string, $msisdn=null,$method=null, $line=null,$logPath=Config::INFO_LOG_PATH) {
        $date = date("Y-m-d H:i:s");
        $fo = fopen($logPath, 'ab');
        if ($fo) {
            fwrite($fo, $date."| ".$msisdn. "|" . $method . " line " . $line . "|" . $string . "\n");
            fclose($fo);
        } else {
            trigger_error("flog Cannot log '$string' to file '$logPath' ", E_USER_WARNING);
        }
    }
      /**
     * Utility to print an array. Strips out passwords.
     *
     * @param array $arr The array to print
     *
     * @return string The array as a string that can be printed
     */
    public function printArray($arr)
    {
        $str = print_r($arr, true);
        $str_replaced = preg_replace(
            "/.password] => (.+)/", "[password] => **********", $str
        );
        $str1 = str_replace("\n", "", $str_replaced);
        $str2 = str_replace("   ", "", $str1);
        $str3 = str_replace("  ", "", $str2);
        return $str3;
    }
    
    public static function handleException(Exception $ex){
        return array('statusCode'=>  Config::STATUS_GENERIC_FAILURE,
                    //'description'=>'An exception Occured,please retry'
                    'description'=>$ex->getMessage());
    }
    
    public static function formulateResponse($code,$message){
        return array('statusCode'=>$code,
                    'description'=>$message);
    }
}