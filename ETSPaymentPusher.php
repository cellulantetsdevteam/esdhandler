<?php

require_once __DIR__ . '/../../lib/Config.php';
require_once __DIR__ . '/../../lib/logger/BeepLogger.php';
require_once __DIR__ . '/../../lib/CoreUtils.php';
require_once __DIR__ . '/../../lib/db/MySQL.php';
require_once __DIR__ . '/../../lib/db/DatabaseUtilities.php';
require_once __DIR__ . '/../../lib/Encryption.php';

/**
 * ETSPaymentPusher file contains a class that formulates post data and
 * 1. Posts to ETS BOOKING engine
 * 2. Posts it to the ESDHandler //electronic signature device
 * PHP VERSION 5.3.6
 *
 * @category  Payment Wrapper
 * @package   PaymentWrapperScripts
 * @author    PHP enthusiast <eric.wanjau@cellulant.com>
 * @copyright 2016 Cellulant Ltd
 * @license   Proprietory License
 * @link      http://www.cellulant.com
 * @date      Mon Nov 14 2016
 */
class ETSPaymentPusher
{
    /**
     * Log class instance.
     *
     * @var object
     */
    private $log;
    private $ETSresponse;
    private $ESDHandlerResponse;
    private $MSISDN;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->log = new BeepLogger();

    }

    public function processRecord(PaymentHandler $data)
    {
        $this->MSISDN=$data->MSISDN;
        $this->log->info(
            Config::INFO, $this->MSISDN, "processing payment with Beep Transaction ID: "
            . $data->beepTransactionID . " and Payer Transaction ID: "
            . $data->payerTransactionID
        );
        
        if($this->postToBookingEngine($data)){
            if($this->postToESDHandler($data)){
               return $this->formulateResponse(Config::PUSH_STATUS_SUCESS,
                            $data,'PUSH_STATUS_SUCESS');
                    }
                else  
                    return $this->formulateResponse(
                                 Config::PUSH_STATUS_FAILED_RETRY,
                                $data,'failed to postToESDHandler');
        }  else {
            return 
            $this->formulateResponse(
            $this->determineHUBStatusCode($this->ETSresponse['result_code']), 
            $data,
            $this->ETSresponse['message']);
               
        }

        
    }
    
    /** Invoke Booking engine url and post params
     * @param PaymentHandler $data
     * @return boolean
     */
    function postToBookingEngine(PaymentHandler $data){
        $payload= static::formulateETSPayload($data);
        $this->log->debug(
                Config::DEBUG,
                $data->beepTransactionID,
                "Posting to: '" . $data->url . "' with parameters: "
                . $this->log->printArray($payload)
       );
       $this->ETSresponse=CoreUtils::httpJsonPost($this->log, 
                 $data->url, 
                 json_encode($payload));
       
        return $this->isSuccessBEPOST();
    }
    
    
    public function isSuccessBEPOST(){
        // EXPECTING { "result_code": 0, "data": "Hello, ", "message": "OK"}
        if(!empty($this->ETSresponse)&
                $this->ETSresponse['result_code']==0){
           $this->log->info(Config::INFO, '','postToBookingEngine successful');
            return true;
        }else {
            $this->log->info(Config::ERROR,  '',
                    'error code '.$this->ETSresponse['result_code']);
            return FALSE;    
                    }
    }
    public function postToESDHandler(PaymentHandler $data){
             $payload= static::formulateESDPayload($data);
        $this->log->debug(
                Config::DEBUG,
                $data->beepTransactionID,
                "Posting to: '" . $data->url . "' with parameters: "
                . $this->log->printArray($payload)
       );

       $this->ESDHandlerResponse=self::httpJsonPost($payload,
                ETSConfigs::ESD_HANDLER_URL,
                $this->log);
         if($this->ESDHandlerResponse['statusCode']==200){
             return true;
         }
        else {return false;}
    }
    ############ OTHER FUNCTIONS ############################
    static function formulateETSPayload(PaymentHandler $data){
        return   array(
            'api_key' => $data->apiUserName,
            'beepTransactionID' => $data->beepTransactionID,
                'payerTransactionID' => $data->payerTransactionID,
                'amount' => $data->amount,
                'orderNumber'=>$data->accountNumber,
                 'MSISDN' => (string) $data->MSISDN,
                 "paymentMode"=>"Mobile Money",
                 'message'=>$data->narration,
        );
    }

    static function formulateESDPayload(PaymentHandler $data){
        return array(
            'function'=>'Core.logESDRequest',
            'payload'=>array(
                'credentials' => array(
                    'api_key' => $data->apiUserName,
                ),
            'packet' => array(
                'beepTransactionID' => $data->beepTransactionID,
                'payerTransactionID' => $data->payerTransactionID,
                'amount' => $data->amount,
            //    'currencyCode' => $data->currencyCode,
                'orderNo'=>$data->accountNumber,
                //'customerName'=>$data->customerName,
                'invoiceNumber' => $data->invoiceNumber,
                'MSISDN' => (string) $data->MSISDN,
                  //'narration' => $data->narration,
                //'payerClientCode' => $data->payerClientCode,
               // 'datePaymentReceived' =>$data->paymentDate,
               // 'serviceID' => $data->serviceID,
                

            )
        ));
    }
    
    public  function formulateResponse($code, PaymentHandler $data,$description){
        $response=
        array(
          'statusCode'=>$code,
            'statusDescription'=>$description,
          'etra'=>array('ETS'=>$this->ETSresponse,'ESD'=>$this->ESDHandlerResponse),
            'beepTransactionID'=>$data->beepTransactionID,
           'payerTransactionID'=>$data->payerTransactionID, 
           
            );
            $this->log->info(Config::INFO,$data->MSISDN,'****RESPONSE'.json_encode($response,true));
            return $response;
    }
    public static function httpJsonPost($data,$url,  BeepLogger $log){
     
    $data_string = json_encode($data);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);
    curl_close($ch);
    $log->debug(
                Config::DEBUG,
                  '',
                "****response".print_r($result,true)
       );
    return json_decode($result,true);
    }
    
    public function determineHUBStatusCode($etsResultCode=null) {
    switch(true) {
    case array_key_exists($etsResultCode, ETSConfigs::$ETS_RETRY_STATUSES);
         $hubStatusCode=  Config::PUSH_STATUS_FAILED_RETRY;
        break;
    case array_key_exists($etsResultCode, ETSConfigs::$ETS_ESCALATE_STATUSES);
         $hubStatusCode=  Config::PUSH_STATUS_PAYMENT_ESCALTE;//219
        break;
    case array_key_exists($etsResultCode, ETSConfigs::$ETS_REJECTED_STATUSES);
         $hubStatusCode=  Config::PUSH_STATUS_PAYMENT_REJECTED;//141
        break;
    default:
        $hubStatusCode=Config::PUSH_STATUS_FAILED_RETRY;
        
    }
    
        return $hubStatusCode;
    }
}
class ETSConfigs{
  // const ESD_HANDLER_URL='http://127.0.0.1/esdhandler/index.php';
   const ESD_HANDLER_URL='https://41.188.154.187/esdhandler/index.php';
   
    static $ETS_RETRY_STATUSES=array(
        1000=>'INTERNAL_ERROR');
    
    static $ETS_ESCALATE_STATUSES=array( 
        3001=>'INVALID_API_KEY',
        3002=>'INVALID_CREDENTIALS');
    
    static $ETS_REJECTED_STATUSES=array(
        6005=>'ORDER_EXPIRED',
        6002=>'INVALID_ORDER_ID'
        );
}