<?php
namespace App\Lib\Db;


require_once __DIR__ . '/../../../vendor/autoload.php';

use App\Lib\Config;
use \PDO;
use \PDOException;
use Exception;

class PDODbConnection
{
    /**
     * Database connection variable
     */
    private $dbConnection;
    /**
     * logging
     */
    private $log;

    public function __construct()
    {
        /**
         * dsn this represents Data source name,specifies the host,port and database name
         */
        $dsn = 'mysql:host=' . Config::HOST . ';
                port=' . Config::PORT . ';
                dbname=' . Config::DATABASE . '';

        try {
            //create an instance of the PDO
            $this->dbConnection = new PDO(
                $dsn,
                Config::USER,
                Config::PASSWORD,
                array(
                   //
                    PDO::ATTR_PERSISTENT => true //allow persistent connections on pdo
                )
            );

            //enable listening for all PDO exceptions
            $this->dbConnection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
         
        } catch (PDOException $pdoException) {
            \App\Lib\CoreUtils::flog(
                "DB error" . $pdoException->getMessage(),'',__METHOD__,__LINE__,
                Config::FATAL_LOG_PATH);
            throw new Exception('db connection error', 500);
        }
    }


    /**
     * Gets an instance of the dbConnection
     * @return PDO of the connection
     */
    public function getConnection()
    {
        return $this->dbConnection;
    }

}