<?php

//autoload  classes
require_once __DIR__ . '/vendor/autoload.php';

//reference classes in out local namespace
use App\Lib\CoreUtils;
use App\Src\CoreAPI;
use App\Lib\Config;


$apiFunctions = array(
    'Core.logESDRequest'=>array('class'=>'CoreAPI','method'=>'logESDRequest'),    
    
);

//receive the post
$decodedRequest = CoreUtils::receivePost();

//sanitize the input
if (isset($decodedRequest['status']['statusCode'])
    && $decodedRequest['status']['statusCode']
    == \App\Lib\Config::STATUS_GENERIC_FAILURE
) {
    echo json_encode($decodedRequest);
    die();
}

//Instantiate the router class and start processing the request
if (!isset($decodedRequest['function']) ||
    empty($decodedRequest['function'])
) {
    $genericErrorResponse['status']['statusCode'] = \App\Lib\Config::STATUS_GENERIC_FAILURE;
    $genericErrorResponse['statusDescription'] = 'Function not Specified';
    $genericErrorResponse['results'] = array();
    echo json_encode($genericErrorResponse);
    die();
}

//log at this point

//confirm that we have a payload
if (!isset($decodedRequest['payload']) ||
    empty($decodedRequest['payload'])
) {
    $genericErrorResponse['status']['statusCode'] = \App\Lib\Config::STATUS_GENERIC_FAILURE;
    $genericErrorResponse['statusDescription'] = 'Payload is missing';
    $genericErrorResponse['results'] = array();
    die();
}

//we found the class
if(array_key_exists($decodedRequest['function'], $apiFunctions)) {
    
$function=$decodedRequest['function'];

    $apiRouter = new $apiFunctions[$function]['class'];
    //call the function inside that class with the decoded request
    $response=call_user_func_array(
        array($apiRouter,$apiFunctions[$function]['method']),
        array($decodedRequest['payload'])
    );
    CoreUtils::flog(json_encode($response));
    echo json_encode($response);
    die();
} else {
    $genericErrorResponse['status']["statusCode"]
        = \App\Lib\Config::STATUS_GENERIC_FAILURE;
    $genericErrorResponse['status']["statusDescription"]
        = "Function '" . $function
        . "' does not exist. Please refer to the API documentation for available functions.";
    $genericErrorResponse["results"] = array();
    echo json_encode($genericErrorResponse);
    die();
}

