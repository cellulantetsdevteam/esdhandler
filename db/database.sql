
CREATE DATABASE `esd_signatures` ;
USE `esd_signatures`;

DROP TABLE IF EXISTS `esdSignatures`;
CREATE TABLE `esdSignatures` (
  `esdSignID` int(11) NOT NULL AUTO_INCREMENT,
  `companyID` bigint(11) NOT NULL,
  `beepTransactionID` bigint(11) NOT NULL,
  `amount` bigint(11) NOT NULL,
  `orderNo` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `esdSignature` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime DEFAULT NULL,
  `dateAcknowledged` datetime DEFAULT NULL,
  PRIMARY KEY (`esdSignID`),
  UNIQUE KEY `beepTransactionID` (`beepTransactionID`)
) ENGINE=InnoDB;