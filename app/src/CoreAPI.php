<?php

//autoload classes
require_once __DIR__ . '/../../vendor/autoload.php';

use App\Lib\BenchMark;
use \App\Lib\CoreUtils;
use \App\Lib\Config;
use \App\Lib\Db\PDODbConnection;


class CoreAPI {

    
    /**
     * @var BenchMark used to benchmark function processing times
     */
    private $tat;

    /**
     * @var string customers phone number
     */
    private $MSISDN;

    /**
     * MySQL return code if it encounters a duplicate
     * value for column key
     */
    const MYSQL_DUPLICATE_KEY_CODE = 1062;

    /**
     * Flag used to indicate this is a duplicate transaction
     */
    const DUPLICATE_KEY_FLAG = 'duplicate';

    /**
     * @var PDO object
     */
    private $pdo; 
    private $responseArray;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->tat = new BenchMark();
      }

      public function db_connect(){
        $this->tat->start($method='PDODbConnection');
        $this->pdo = new PDODbConnection();
        $this->tat->logTotalTAT($method='PDODbConnection');
      }

      /**
     * This function is called to ..
     * @param $request array the decoded post Request from client
     */
    public function logESDRequest($payload) {
        $this->tat->start(__METHOD__);
       
        if($this->validate($payload['packet'],__FUNCTION__)==FALSE) {
             CoreUtils::flog($this->responseArray['description'],'',__METHOD__,
                     __LINE__,  Config::ERROR_LOG_PATH);
                        return $this->responseArray;}
        try {
          
        $res=$this->insertESDRequest($payload['packet']);
        
        } catch (Exception $ex) {
               CoreUtils::flog('code:'.$ex->getCode()
                       .' msg '.$ex->getMessage(),'',__METHOD__,
                     __LINE__,  Config::ERROR_LOG_PATH);
               
            if($ex->getCode()==Config::STATUS_MYSQL_DUPLICATE_RECORD){
                $res=  \App\Lib\CoreUtils::formulateResponse(Config::STATUS_SUCCESS, 
                        $message='duplicate beepTransactionID');
            }else{
            return CoreUtils::handleException($ex);}
        }
        $this->tat->logFunctionTAT(__METHOD__);
        return $res;
    }
    
    private function insertESDRequest($packet){
         $this->db_connect();
         $con=  $this->pdo->getConnection();
         $query="INSERT INTO `esdSignatures` 
             (`companyID`, `beepTransactionID`, `amount`, orderNo,status)
            VALUES (?, ?, ?, ?,?);";
            //prepare the query for execution
            $preparedStatement = $con->prepare($query);
            $params = array(
                'iiisi', //data type string
                Config::DEFAULT_COMPANY_ID, $packet['beepTransactionID'],
                $packet['amount'],
                $packet['orderNo'], 
                Config::STATUS_UNPROCESSED);

            //log the query after formatting it
            CoreUtils::flog(" query: " . CoreUtils::formatQueryForLogging($query, $params), '',__METHOD__,
                    __LINE__,  Config::DEBUG_LOG_PATH
            );

            //unset the first element.we only need this for logging
            unset($params[0]);

            //reindex the params array and execute the query
            if($preparedStatement->execute(array_values($params))){
                
                $this->responseArray=CoreUtils::formulateResponse(
                Config::STATUS_SUCCESS,'success logged '.$packet['beepTransactionID']);
                CoreUtils::flog('succsess'.json_encode($this->responseArray),'',
                        __METHOD__,__LINE__);
            
            }
            else{
                 CoreUtils::flog('fail','',__METHOD__,__LINE__);
                $this->responseArray=CoreUtils::formulateResponse
            ($code=0, $message='insert error');}
            //get the lastInsertID
           // $lastInsertID = $pdoConnection->lastInsertId();
            return $this->responseArray;
    }
    
    private function validate($packet,$function){
        $error=null;
        foreach(Config::$VALIDATION_PARAMS[$function] as $key) {
            if(array_key_exists($key, $packet)){
               if(empty($packet[$key])){
                   $error.=':-( empty '.$key;
               }
            }  else {
             $error.=' missing '.$key;    
            }
        }
      
        if($error!=null){
            $this->responseArray=CoreUtils::formulateResponse(
                    0,$error);
            
            return FALSE;        
        }
        else return true;
    }
}