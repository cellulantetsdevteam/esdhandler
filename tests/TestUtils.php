<?php
class Configs{
     const HUB_ENDPOINT_URL = "http://197.159.100.249:9000/hub/services/paymentGateway/XML/";
    public static $HUB_CREDENTIALS= array(
                'username'=>'ets_tz',
                'password'=>'!23qweASD',
                 'serviceID'=>656,
                 'paymentMode'=>'CASH',
                 'currencyCode'=>'TZS',
                 'payerClientID'=>237,
        );
    
}
/**
 * Description of TestUtils
 *
 * @author ericwanjau
 */
class TestUtils {
    //put your code here
    public static function httpPost($data,$url) {
        
        $payload = http_build_query($data);
    echo "URL: ".$url
    . " \n payload \n :",$payload,"\n";

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $response = curl_exec($curl);
    if (curl_errno($curl)) {
        echo "Error : ", print_r(curl_errno($curl),true);
    }
    curl_close($curl);

    echo "*************\n";
    print_r($response);
    return $response;

    }
    public static function httpJsonPost($data,$url){
     
    $data_string = json_encode($data);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);
    print_r($result);
    }
    
    public static function invokeBEEP($beepFunction, $packet) {
           $payload = array(
            'credentials' =>array(
                    'username'=> Configs::$HUB_CREDENTIALS['username'],
                    'password'=>  Configs::$HUB_CREDENTIALS['password']),
            'packet' => array($packet)
        );
        $response = null;
        
        try {
            $client = new IXR_Client(Configs::HUB_ENDPOINT_URL);
           // $client = new IXR_ClientSSL(Configs::HUB_ENDPOINT_URL);
            $client->debug = false;
            $client->query($beepFunction,$payload);
            $response = $client->getResponse();
            return $response;
        } catch (Exception $ex) {
            return;
        }
    }
}
